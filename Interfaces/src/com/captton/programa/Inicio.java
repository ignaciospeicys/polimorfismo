package com.captton.programa;

import java.util.ArrayList;

import com.captton.inter.*;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		IAnotador l = new Lapicera();
		l.anotar("Hola soy lapicera (desde un anotador)");
		
		Lapicera lapic = new Lapicera();
		lapic.anotar("desde instancia Lapicera");
		
		System.out.println(" ");
		
		IAnotador C = new Computadora();
		C.anotar("Soy computadora");
		
		Computadora compu = new Computadora();
		compu.anotar("desde instancia computadora\n");
		
		compu.encender();
		
		ArrayList<IAnotador> anotadores = new ArrayList<IAnotador>();
		anotadores.add(l);
		anotadores.add(C);
		
		System.out.println(" ");
		
		for(IAnotador iterador: anotadores) {
			
			iterador.anotar("el mensaje");
			
			if(iterador instanceof Lapicera) {
			
			System.out.println("Es Lapicera");
			} else {
				System.out.println("Es Computadora");
				((Computadora)iterador).encender();
			}
		}	
	}
}
